#include "wordlink.h"
#include "wordlink_options.h"
#include "wordlink_utility.h"
#include "list.h"

/****************************************************************************
* Function readRestOfLine() is used for buffer clearing. Blackboard source: 
* "Course Documents" > "FAQ" > "Alternative to non-standard fflush(stdin);"
****************************************************************************/
void readRestOfLine()
{
   int c;

   /* Read until the end of the line or end-of-file. */   
   while ((c = fgetc(stdin)) != '\n' && c != EOF)
      ;

   /* Clear the error and end-of-file flags. */
   clearerr(stdin);
}


/****************************************************************************
* Initialises the dictionary to a safe empty state.
****************************************************************************/
void systemInit(DictionaryType* dictionary)
{
   int i;

   /* Loop through the headsWords array and 
      initialize all elements to NULL */
   for(i = 0; i < ALPHABET_LEN; i++)
   {
      dictionary->headWords[i] = NULL;
   }

   /* Set all listCounts array elements to 0 */
   memset(dictionary->listCounts, 0, ALPHABET_LEN);
   /* Set totalWords counter to 0 */
   dictionary->totalWords = 0;

   printf("\n\nSystem Initialized.\n");
}


/****************************************************************************
* Loads all data into the dictionary.
****************************************************************************/
int loadData(DictionaryType* dictionary, char* filename)
{
   char word[WORD_LENGTH], *wordCopy, ch = 'a';
   int listQty = 0, headWordsElement = 0, listCnt = 1;
   int listCountsElement = 0, nodePosition;
   FILE *fp;

   /* Open and read from passed in file */
   if((fp = fopen(filename, "r")) == NULL)
   {
      /* Print error and exit if there is an error opening the file */
      fprintf(stderr, "Error opening file.\n"); 
      exit(EXIT_FAILURE);  
   }

   /* Get each word line by line from the file */
   while(fgets(word, WORD_LENGTH, fp) != NULL) 
   {  
      /* Create a temp node "wordCopy" that stores the entered word */
      wordCopy = malloc((strlen(word) + 1) * sizeof(char));
      /* Copy the word into the temp node */
      strcpy(wordCopy, word);

      /* Reset ch and all counters if text file loops back to start 
         of the alphabet */
      if(*word != ch && *word == 'a')
      {
         ch = 'a';
         listQty = 0;
         headWordsElement = 0; 
         listCnt = 1;
         listCountsElement = 0;
      }

      /* Check if the first letter of the word matches the ascii value */
      if(*word == ch)
      {
         /* Get the node position for addList() */
         nodePosition = getRandNumber(listCnt);
         /* Call to the addList() */
         addList(dictionary, wordCopy, headWordsElement, nodePosition);
         /* Tally up the amount of words in each alphabetic list */
         listQty++;
         /* Increment the list count to generate random position numbers */ 
         listCnt++;
         /* Assign the list quantity ot listCounts[] */
         dictionary->listCounts[listCountsElement] = listQty;
      }

      else
      {
         /* Increment up to the next ascii value */
         ch++; 

         /* Check if the first letter of the word does not 
            match the ascii value - this is a special case, for example if 
            there are no words begining with the current ascii char value */
         if(*word != ch)
         {  
            /* Reset each listCount back to 0 */
            listQty = 0; 
            /* Increment up to the next listCounts slot */
            listCountsElement++; 
            /* Assign the list quantity ot listCounts[] */
            dictionary->listCounts[listCountsElement] = listQty;
            /* Increment up to the next headWords slot */
            headWordsElement++; 
            /* Increment up to the next ascii value */
            ch++;
         }
         
         /* Increment up to the next headWords slot */
         headWordsElement++; 
         /* Reset each listQty back to 0 */
         listQty = 0; 
         /* Reset each listCnt back to 0 */
         listCnt = 1; 
         /* Get the node position for addList() */
         nodePosition = getRandNumber(listCnt);
         /* Call to the addList() */
         addList(dictionary, wordCopy, headWordsElement, nodePosition);
         /* Tally up the amount of words in each alphabetic list */
         listQty++; 
         /* Increment the list count to generate random position numbers */ 
         listCnt++;
         /* Increment up to the next listCounts slot */
         listCountsElement++; 
         /* Assign the list quantity to listCounts[] */
         dictionary->listCounts[listCountsElement] = listQty;
      }
   } /* End of while loop */

   /* Close the file */
   fclose(fp); 

   printf("Data loaded.\n");

   return SUCCESS;
}


/****************************************************************************
* Deallocates memory used in the dictionary.
****************************************************************************/
void systemFree(DictionaryType* dictionary)
{
   WordLinkTypePtr current, head = NULL, tempNode;
   int i;
   
   /* Loop through each element of the headWords array */
   for(i = 0; i < ALPHABET_LEN; i++)
   {  
      /* Set head to the ith headWordsElement of the headWords array */
      head = dictionary->headWords[i];
      /* Set current to head */
      current = head; 
   
      /* While not pointing to the end of the list */
      while(current != NULL) 
      { 
         /* Set current->nextWord to tempNode */
         tempNode = current->nextWord;
         /* Free the current word */
         free(current->word);
         /* Free the current pointer */
         free(current);
         /* Set tempNode to current */
         current = tempNode;
      } 
   }

   printf("\nProgram terminated and memory deallocated.\n\n"); 
}
