/****************************************************************************
* COSC2138/CPT220 - Programming Principles 2A
* SP2 2015 Assignment #2 - word link program
* Full Name        : Paolo Felicelli
* Student Number   : s3427174
* 
* Start up code provided by the C Teaching Team
****************************************************************************/

#include "wordlink.h"
#include "wordlink_options.h"
#include "wordlink_utility.h"
#include "list.h"

int main(int argc, char* argv[])
{
   DictionaryType dictionary;
   int min = 1, max = 7, result = 0;

   /* Check if 2 command line arguments are entered */
   if(argc != 2) 
   {
      printf("File not found.\n");
      return EXIT_FAILURE;
   }

   /* Function calls to initialize the system and load the dictionary data */
   systemInit(&dictionary);
   loadData(&dictionary, argv[1]); 
   /* displayList(&dictionary);  Used for testing */

   while(result != 7)
   {
      /* Function call to display the menu dialog */
      displayMenu();
      /* Function call to get an int from user for game options */
      getInteger(&result, INPUT_LENGTH, min, max);

      switch(result)
      {
         case 1:
            onePlayerGame(&dictionary);
         break;

         case 2:
            twoPlayerGame(&dictionary);
         break;

         case 3:
            dictionarySummary(&dictionary);
         break;

         case 4:
            searchDictionary(&dictionary);
         break;

         case 5:
            addDictionaryWord(&dictionary);
         break;

         case 6:
            saveDictionary(&dictionary, argv[1]);
         break;

         case 7:
            systemFree(&dictionary);
         break;
      }  
   }

   return EXIT_SUCCESS;
}


/****************************************************************************
* getInteger(): An interactive integer input function.
* This function prompts the user for an integer using a custom prompt. A line
* of text is accepted from the user using fgets() and stored in a temporary
* string. When the function detects that the user has entered too much text,
* an error message is given and the user is forced to reenter the input. The
* function also clears the extra text (if any) with the readRestOfLine()
* function.
* When a valid string has been accepted, the unnecessary newline character
* at the end of the string is overwritten. The function then attempts to 
* convert the string into an integer with strtol(). The function checks to
* see if the input is numberic and within range.
* Finally, the temporary integer is copied to the integer variable that is 
* returned to the calling function.
*****************************************************************************
* Note: This function is a variation of the getInteger() function provided 
* by RMIT. It has been modified to be made of use for this assignment
****************************************************************************/
int getInteger(int* integer, unsigned length, int min, int max)
{
   int finished = FALSE, tempInteger = 0;
   char tempString[TEMP_STRING_LENGTH + EXTRA_CHARS], *endPtr;

   /* Continue to interact with the user until the input is valid. */
   do
   {
      /* Provide a custom prompt. */
      printf("Select your option (1 - 7): ");
      
      /* Accept input. "EXTRA_CHARS" is for the \n and \0 characters. */
      fgets(tempString, length + EXTRA_CHARS, stdin);

      /* A string that doesn't have a newline character is too long. */
      if(tempString[strlen(tempString) - 1] != '\n')
      {
         printf("Input was too long.\n");
         readRestOfLine();
      }

      else
      {
         /* Overwrite the \n character with \0. */
         tempString[strlen(tempString) - 1] = '\0';

         /* Convert string to an integer. */
         tempInteger = (int) strtol(tempString, &endPtr, 10);

         /* Validate integer result. */
         if(strcmp(endPtr, "") != 0)
         {
            printf("Input was not numeric.\n");
         }

         else if(tempInteger < min || tempInteger > max)
         {
            printf("Input was not within range %d - %d.\n", min, max);
         }

         else
         {
            finished = TRUE;
         }
      }
   } while(finished == FALSE);

   /* Make the result integer available to calling function. */
   *integer = tempInteger;

   return SUCCESS;
}
