/****************************************************************************
* COSC2138/CPT220 - Programming Principles 2A
* SP2 2015 Assignment #2 - word link program
* Full Name        : Paolo Felicelli
* Student Number   : s3427174
* 
* Start up code provided by the C Teaching Team
****************************************************************************/

#include "wordlink.h"
#include "wordlink_options.h"
#include "wordlink_utility.h"
#include "list.h"

/* isOnePlayerGame is used as a flag. It is flipped to true at the start of 
   one player game for the playerGuess() function to know whether it's in one 
   player mode or not. */
int isOnePlayerGame;
/* playerToggle acts as a 3 way toggle switch so that playerGuess() knows 
   which player to process */
int playerToggle;

/****************************************************************************
* Menu option #1: 1 Player Game
* Game between human and computer controlled players.
* 
* Note: Additional modularisation is strongly recommended here.
* Extra candidate functions include playerGuess(), resetGame(), etc.
****************************************************************************/
void onePlayerGame(DictionaryType* dictionary)
{
   char prompt[PROMPT_LENGTH];

   /* Call to reset the dictionary */
   resetGame(dictionary);

   printf("\n1 Player Game\n");
   printf("-------------\n\n");

   /* Flip isOnePlayerGame global to true so that playerGuess knows to call 
      computerGuess during a one player game */
   isOnePlayerGame = TRUE;

   /* Copy the prompt string into the prompt array */
   strcpy(prompt, "Player: Enter a word (1-20 char, blank line to quit): ");

   /* Function call to prompt the player for a word */
   playerPrompt(dictionary, prompt);
}


/****************************************************************************
* Menu option #2: 2 Player Game
* Game between two human-controlled players.
* 
* Note: Additional modularisation is strongly recommended here.
* Extra candidate functions include playerGuess(), resetGame(), etc.
****************************************************************************/
void twoPlayerGame(DictionaryType* dictionary)
{
   char prompt[PROMPT_LENGTH];
   int result, isTrue = TRUE;

    /* Call to reset the dictionary */
   resetGame(dictionary);

   printf("\n2 Player Game\n");
   printf("-------------\n\n");

   /* Flip isOnePlayerGame to false so that playerGuess() processes 
      the game as a 2 player game and knows not to call the computerGuess() 
      function */
   isOnePlayerGame = FALSE;

   while(isTrue)
   {
      /* Copy the prompt string for player 1 into the prompt array */
      strcpy(prompt, "Player 1: Enter a word (1-20 char,"
                     " blank line to quit): ");
      
      /* Flip playerToggle to PLAYER_ONE so playerGuess() knows which player 
         to process */
      playerToggle = PLAYER_ONE;

      /* Function call to prompt player 1 for a word */
      result = playerPrompt(dictionary, prompt);
      
      /* If result fails, break out of loop and return to menu */
      if(result == FAILURE)
      {
         break;
      }
             
      /* Copy the prompt string for player 2 into the prompt array */
      strcpy(prompt, "Player 2: Enter a word (1-20 char,"
                     " blank line to quit): ");
      
      /* Flip playerToggle to PLAYER_TWO so playerGuess() knows which player 
         to process */
      playerToggle = PLAYER_TWO;

      /* Function call to prompt player 2 for a word */
      result = playerPrompt(dictionary, prompt);
      
      /* If result fails, break out of loop and return to menu */
      if(result == FAILURE)
      {
         break;
      }  
       
   } /* End of while loop */
}


/*****************************************************************************
* While loop mechanism to prompt the player for a word with length and \n 
* validation. Also makes a function call to the playerGuess() function.
*****************************************************************************/
int playerPrompt(DictionaryType* dictionary, char *prompt)
{
   char word[WORD_LENGTH];
   int result, isTrue = TRUE, i, isFound = FALSE;
   
   while(isTrue)
   {    
      /* Provide a custom prompt. */
      printf("%s", prompt);

      /* Get the word from the user input */
      fgets(word, WORD_LENGTH + EXTRA_CHARS, stdin);

      /* Function call to clearStream() */
      clearStream(word);

      /* Check the input is less than 20 characters */
      if(word[strlen(word) - 1] != '\n')
      {  
         printf("Entry must be less than 20 characters.\n");
         continue;
      }

      /* Flip back to false if while loop re-starts because of detected int */
      isFound = FALSE;

      /* Loop through user input and check if it contains numbers */
      for(i = 0; i < strlen(word); i++)
      {
         if(isdigit(word[i]) || ispunct(word[i]))
         {
            isFound = TRUE;
            break;
         }
      }

      /* If numbers or charcters are found, break out of 
         loop and return to menu */
      if(isFound == TRUE)
      {
         printf("Input must be any letter (a - z).\n");
         continue;
      }

      /* If user enters a blank line, return to menu */
      if(word[0] == '\n')
      {
         printf("\nYou entered a blank line to give up."
                " Better luck next time.\n");
         return FAILURE;
      }

      /* Call to playerGuess() */
      result = playerGuess(dictionary, word);

      /* If playerGuess() returns failure, return to menu */
      if(result == FAILURE)
      {
         return FAILURE;
      }

      /* playerGuess() is successful, break out of the loop */
      if(result == SUCCESS && isOnePlayerGame == TRUE)
      {
         isTrue = TRUE;
      }

      else
         /* Flip isTrue to false and exit the loop */
         isTrue = FALSE;

   } /* End of while loop */

   return SUCCESS;
}


/*****************************************************************************
* playerGuess() loops through the entire dictionary and attempts to find a 
* match between the guessed word and a word in the dictionary. It also checks 
* whether the user has previously guessed a word.
*****************************************************************************/
int playerGuess(DictionaryType* dictionary, char *enteredWord)
{
   int i, isFound = FALSE, isTrue = TRUE, result = SUCCESS;
   char lastChar, *newString;
   WordLinkTypePtr current, head = NULL;

   /* Loop through each element of the headWords array */
   for(i = 0; i < ALPHABET_LEN; i++)
   {  
      /* Assign each element to the head pointer */
      head = dictionary->headWords[i]; 
      /* Set current to head */
      current = head;
   
      /* Loop through the linked lists and find a match 
         with the entered word */
      while(current != NULL && isTrue) 
      { 
         /* Compare the 2 words and check if the word has been used */
         if(strcmp(current->word, enteredWord) == 0 
                   && current->usedFlag != MARKED)
         {
            /* If word is found, flip isFound to TRUE */
            isFound = TRUE;
            /* Mark it as used so it can't be used again */
            current->usedFlag = MARKED;
            /* Function call to get the last char of the word */
            lastChar = getLastChar(enteredWord);

            /* Function call to computerGuess() only 
               if in one player mode */
            if(isOnePlayerGame == TRUE)
            {
               result = computerGuess(dictionary, lastChar);
            }

            /* If result is FAILURE, return to menu */
            if(result == FAILURE)
            {
               return FAILURE;
            }

            /* Flip isTrue to FALSE */
            isTrue = FALSE;
         }

         /* Check if the user tries to use a word that has 
            already been entered, if so it's game over */
         else if(strcmp(current->word, enteredWord) == 0 
                 && current->usedFlag == MARKED)
         {
            /* Function call to remove the \n character from the word*/
            newString = removeNewline(enteredWord);

            printf("\nGame over, you have already used the word \"%s\".", 
                     newString);
            return FAILURE;
         }

         /* Point to the next word */
         current = current->nextWord;
      } /* End of while loop */
   } /* End of for loop */

   /* Function call to remove the \n character from the word*/
   newString = removeNewline(enteredWord);

   /* Check 1f player enters a word that does not exist while playing
      against the computer */
   if(isFound == FALSE && isOnePlayerGame == TRUE)
   {
      printf("\n\"%s\" is not in the dictionary. Computer wins!\n", 
         newString);
      return FAILURE;
   }

   /* Check 1f player 1 enters a word that does not exist */
   if(isFound == FALSE && playerToggle == PLAYER_ONE)
   {
      printf("\n\"%s\" is not in the dictionary. Player 2 wins!\n", 
               newString);
      return FAILURE;
   }      

   /* Check 1f player 2 enters a word that does not exist */
   if(isFound == FALSE && playerToggle == PLAYER_TWO)
   {
      printf("\n\"%s\" is not in the dictionary. Player 1 wins!\n", 
               newString);
      return FAILURE;
   }        

   return SUCCESS;
}


/*****************************************************************************
* computerGuess() finds a word in the dictionary that starts with the last 
* char of the user guessed word, if the computer exhaust all it's guesses, 
* the user wins.
*****************************************************************************/
int computerGuess(DictionaryType *dictionary, char lastLetter) 
{ 
   WordLinkTypePtr current, head = NULL;
   int i, isFound = FALSE, isTrue = TRUE;
   char *currWord, *newString;

   /* Flip playerToggle to COMPUTER_PLAYER so playerGuess() knows which 
      player to deal with */
   playerToggle = COMPUTER_PLAYER;
  
   /* Loop through each element of the headWords array */
   for(i = 0; i < ALPHABET_LEN; i++)
   {  
      /* Assign each element to the head pointer */
      head = dictionary->headWords[i]; 
      /* Set current to head */
      current = head;
   
      /* Loop through the linked lists */
      while(current != NULL && isTrue) 
      { 
         /* Give current->word a variable for readability */
         currWord = current->word;
         
         /* Check if 1st letter of the current word matches the last letter
            of the users guessed word and that the current word has not 
            already been used */
         if(currWord[0] == lastLetter && current->usedFlag != MARKED)
         {
            /* If word is found, flip isFound to TRUE */
            isFound = TRUE;
            /* If the word is found, mark it so it can't be used again */
            current->usedFlag = MARKED;
            /* Function call to remove the \n character from the word*/
            newString = removeNewline(currWord);

            printf("Computer selects word \"%s\".\n\n", newString); 
            /* Flip isTrue to false */
            isTrue = FALSE;
         }
         
         /* Point to the next word in the list */
         current = current->nextWord;

      } /* End of while loop */ 
   } /* End of for loop */
   
   /* If the computer has exhausted guesses */
   if(isFound == FALSE)
   {
      printf("\nComputer could not make a guess. You win!\n");
      return FAILURE;
   }      

   return SUCCESS;
}  


/*****************************************************************************
* resetGame loops through the entire dictionary and resets MARKED words
* to UNUSED (0) at the start of a 1 and 2 player game.
*****************************************************************************/
void resetGame(DictionaryType *dictionary) 
{ 
   int i;
   WordLinkTypePtr current, head = NULL;
  
   /* Loop through each element of the headWords array */
   for(i = 0; i < ALPHABET_LEN; i++)
   {  
      /* Assign each element to the head pointer */
      head = dictionary->headWords[i]; 
      /* Set current to head */
      current = head;
   
      /* Loop through dictionary and reset usedFlag to 0 */
      while(current != NULL) 
      { 
         /* Set all flags as unused */
         current->usedFlag = UNUSED;
         /* Point to the next node */
         current = current->nextWord;
      } 
   }
}  


/****************************************************************************
* Menu option #3: Dictionary Summary
* Displays statistics on the words in the dictionary.
****************************************************************************/
void dictionarySummary(DictionaryType* dictionary)
{
   int i, j, numOfCols = 2, listSlot = 12, half = 2;
   char ch1 = 'a', ch2 = 'n';

   printf("\nDictionary Summary\n");
   printf("------------------\n\n");

   printf("+-----------+------------+\n");

   /* Outer loop is responsible for left column */
   for(i = 0; i < ALPHABET_LEN/half; i++)
   {
      printf("| %c:  %4d  |", ch1, dictionary->listCounts[i]);
      /* Increment up the alphabet */
      ch1++;

      /* Inner loop is responsible for right column */
      for(j = 1; j < numOfCols; j++)
      {
          printf("  %c:  %4d  |\n", 
                   ch2, dictionary->listCounts[j + listSlot]);
          /* Increment up the alphabet */
          ch2++; 
          /* Increment up the listCounts array */
          listSlot++;
          break;
      }
   }
   printf("+-----------+------------+\n\n");         
}


/****************************************************************************
* Menu option #4: Search Dictionary
* Prompts the user for a dictionary word and reports if the word is in the
* dictionary.
****************************************************************************/
void searchDictionary(DictionaryType* dictionary)
{
   char enteredWord[WORD_LENGTH], *newString;
   int isTrue = TRUE, result, i, isFound = FALSE;

   printf("\nSearch Dictionary\n");
   printf("-----------------\n\n");

   while(isTrue)
   {    
      /* Provide a prompt. */
      printf("Enter a word (1-20 char): ");

      /* Get the word from the user input */
      fgets(enteredWord, WORD_LENGTH + EXTRA_CHARS, stdin);

      /* Function call to clearStream() */
      clearStream(enteredWord);

      /* Check if the input is less than 20 characters */
      if(enteredWord[strlen(enteredWord) - 1] != '\n')
      {  
         printf("Entry must be less than 20 characters.\n");
         continue;
      }

      /* If user enters a blank line, return to menu */
      if(enteredWord[0] == '\n')
      {
         printf("\nYou entered a blank line, back to menu.\n");
         break;
      }

      /* Loop through user input and check if it contains numbers */
      for(i = 0; i < strlen(enteredWord); i++)
      {
         if(isdigit(enteredWord[i]) || ispunct(enteredWord[i]))
         {
            isFound = TRUE;
            break;
         }
      }

      /* If numbers or characters are found, break out of 
         loop and return to menu */
      if(isFound == TRUE)
      {
         printf("Input must be any letter (a - z).\n");
         break;
      }

      /* Function call to findWord() */
      result = findWord(dictionary, enteredWord);

      /* Function call to remove the \n character from the word */
      newString = removeNewline(enteredWord);

      /* Check if findWord() is not successful and return back to the menu */
      if(result == FAILURE)
      {
         printf("\n\"%s\" is not in the dictionary.\n", newString);  
         isTrue = FALSE;
      }

      /* If findWord() is successful return back to the menu */
      if(result == SUCCESS)
      {
         printf("\n\"%s\" is in the dictionary.\n", newString);
         isTrue = FALSE;
      }
   } /* End of while loop */
}


/*****************************************************************************
* findWord() function is an extension of the searchDictionary() function.
*****************************************************************************/
int findWord(DictionaryType* dictionary, char *enteredWord)
{
   int i, isTrue = TRUE, isFound = FALSE;
   WordLinkTypePtr current, head = NULL;

   /* Loop through each element of the headWords array */
   for(i = 0; i < ALPHABET_LEN; i++)
   {  
      /* Assign each element to the head pointer */
      head = dictionary->headWords[i]; 
      /* Set current to head */
      current = head;
   
      /* Loop through the linked list and print the contents of the node */
      while(current != NULL && isTrue) 
      { 
         /* Check if the entered word is == to the current word */
         if(strcmp(current->word, enteredWord) == 0)
         {
            /* Flip both booleans */
            isFound = TRUE;
            isTrue = FALSE;
         }

         /* Point to the next word in the list */
         current = current->nextWord;

      } /* End of inner while loop */
   } /* End of for loop */

   /* Check if match is not found */
   if(isFound == FALSE)
   {
      return FAILURE;
   }

   return SUCCESS;
}


/****************************************************************************
* Menu option #5: Add Dictionary Word
* Prompts the user for a new dictionary word to be added to the data
* structure.
****************************************************************************/
void addDictionaryWord(DictionaryType* dictionary)
{
   char enteredWord[WORD_LENGTH];
   int isTrue = TRUE, result, i, isFound = FALSE;

   printf("\nAdd Dictionary Word\n");
   printf("-------------------\n\n");

   while(isTrue)
   {    
      /* Provide a prompt. */
      printf("\nEnter a word (1-20 char): ");

      /* Get the word from the user input */
      fgets(enteredWord, WORD_LENGTH + EXTRA_CHARS, stdin);

      /* Function call to clearStream() */
      clearStream(enteredWord);

      /* Check the input is less than 20 characters */
      if(enteredWord[strlen(enteredWord) - 1] != '\n')
      {  
         printf("Entry must be less than 20 characters.\n");
         continue;
      }

      /* If user enters a blank line, return to menu */
      if(enteredWord[0] == '\n')
      {
         printf("\nYou entered a blank line, back to menu.\n");
         break;
      }

      /* Loop through user input and check if it contains numbers */
      for(i = 0; i < strlen(enteredWord); i++)
      {
         if(isdigit(enteredWord[i]) || ispunct(enteredWord[i]))
         {
            isFound = TRUE;
            break;
         }
      }

      /* If numbers or characters are found, break out of 
         loop and return to menu */
      if(isFound == TRUE)
      {
         printf("Input must be any letter (a - z).\n");
         break;
      }

      /* Function call to findWord() */
      result = findWord(dictionary, enteredWord);

      /* Function call to userAddedWord() */
      userAddedWord(dictionary, enteredWord, result);

      /* Flip isTrue to false */
      isTrue = FALSE;

   } /* End of while loop */
}


/*****************************************************************************
* userAddedWord() is an extension module of the addDictionaryWord() function.
*****************************************************************************/
void userAddedWord(DictionaryType* dictionary, char *enteredWord, int result)
{
   int listCountsElement = 0, headWordsElement = 0, addWordResult = 0;
   int position = 0, i, listQty;
   char *wordCopy, *newString, ch = 'a';

   /* Create a temp node "wordCopy" that stores the entered word */
   wordCopy = malloc((strlen(enteredWord) + 1) * sizeof(char));
   /* Copy the entered word into the temp node */
   strcpy(wordCopy, enteredWord);

   for(i = 0; i < ALPHABET_LEN; i++)
   {
      /* Check if the first letter of the word matches the ascii value */
      if(*enteredWord == ch && result == FAILURE)
      {
         /* Call to the addList() */
         addWordResult = addList(dictionary, wordCopy, 
                                 headWordsElement, position);
         /* Get the list quantity */
         listQty = dictionary->listCounts[listCountsElement];
         /* Add 1 to the list quantity */
         listQty++;
         /* Assign the list quantity ot listCounts[] */
         dictionary->listCounts[listCountsElement] = listQty;
         break;
      }

      /* Increment all elements */
      ch++;
      headWordsElement++;
      listCountsElement++;
   } /* End of for loop */

   /* Function call to remove the \n character from the word */
   newString = removeNewline(enteredWord);   

   /* Check if add word is successful */
   if(addWordResult == SUCCESS)
   {
      printf("\n\"%s\" has been added to the dictionary.\n", newString);
   }      

   /* Check if entered word already exists */
   if(result == SUCCESS && enteredWord[0] != '\n')
   {
      printf("\n\"%s\" already exists in the dictionary.\n", newString);
   }
}


/****************************************************************************
* Menu option #6: Save Dictionary
* Writes the contents of the dictionary back to the original file.
*****************************************************************************
* This option is also responsible for sorting the dictionary back into 
* alphabetic/ascending order and writing it back to file.
****************************************************************************/
int saveDictionary(DictionaryType* dictionary, char* filename)
{
   char *words[SIZE];
   int i;
   static int k = 0;
   WordLinkTypePtr current, head = NULL;
   FILE *fp;
   size_t wordsSize;

   /* Loop through words and init to NULL */
   for(i = 0; i < SIZE; i++)
   {
      words[i] = NULL;
   }

   /* Open file for writing */
   if((fp = fopen(filename, "w")) == NULL)
   {
      fprintf(stderr, "Error opening file\n");
      exit(EXIT_FAILURE);
   }

   /* Loop through each element of the headWords array */
   for(i = 0; i < ALPHABET_LEN; i++)
   {  
      /* Assign each element to the head pointer */
      head = dictionary->headWords[i]; 
      /* Set current to head */
      current = head;
   
      /* Loop through the linked list */
      while(current != NULL) 
      { 
         /* Assign the current word to the current array slot */
         words[k] = current->word;
         /* Increment up the array/count words */
         k++;
         /* Point to the next word */
         current = current->nextWord;
      } 
   }

   /* Get the size of the array */
   wordsSize = k * sizeof(char *) / sizeof(const char *);

   /* Call qsort to sort the array in ascending order */
   qsort(words, wordsSize, sizeof(const char *), compare);

   /* Loop through the words array and get each word */
   for(i = 0; i < wordsSize; i++)
   {
      /* Write the words to the file */
      fprintf(fp, "%s", words[i]);
   }
   
   printf("\nFile has been saved.\n");

   /* Close the file */
   fclose(fp);

   free(current);

   return SUCCESS;
}


/*****************************************************************************
* clearStream() clears the stream by calling readRestOfLine() if there is no 
* new line and there are still pending characters in the stream. 
*****************************************************************************/
void clearStream(char *string)
{
   char *newLine = NULL;

   /* Search the string for the 1st occurance of the \n character */
   newLine = strchr(string, '\n');

   /* If \n not found, call readRestOfLine() */
   if(newLine == NULL)
   {
      readRestOfLine();
   }
}


/*****************************************************************************
* getLastChar() returns a pointer to the last character of the word.
*****************************************************************************/
char getLastChar(char *string)
{
   int len;
   /* Position of last char */
   char *position; 

   /* Get the length of the string */
   len = strlen(string);

   /* Make position point to last char of string */ 
   position = string + len - 2;
   
   return *position;
}


/*****************************************************************************
* removeNewline() removes the \n character from the string
*****************************************************************************/
char *removeNewline(char *string)
{
   int len;

   /* Get the length of the string */
   len = strlen(string);
   
   /* Check if string has \n */
   if(string[len - 1] == '\n')
   {
      /* Repalce it with \0 */
      string[len - 1] = '\0';
   }
   
   return string;
}


/*****************************************************************************
* Function that displays the menu - self explanatory.
*****************************************************************************/
void displayMenu()
{
   printf("\n\nMain menu:\n\n");
   printf("(1) 1 Player game\n");
   printf("(2) 2 Player game\n");
   printf("(3) Dictionary summary\n");
   printf("(4) Search dictionary\n");
   printf("(5) Add dictionary word\n");
   printf("(6) Save dictionary\n");
   printf("(7) Exit\n\n");
}


/*****************************************************************************
* qsort C - string comparison function.
*****************************************************************************/
int compare (const void *a, const void *b)
{
   /* The pointers point to offsets into "array", so we need to
    dereference them to get at the strings. */
   return strcmp(*(const char **) a, *(const char **) b);
}
