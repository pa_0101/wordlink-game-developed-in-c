/****************************************************************************
* COSC2138/CPT220 - Programming Principles 2A
* SP2 2015 Assignment #2 - word link program
* Full Name        : Paolo Felicelli
* Student Number   : s3427174
* 
* Start up code provided by the C Teaching Team
****************************************************************************/

#ifndef WORDLINK_OPTIONS_H
#define WORDLINK_OPTIONS_H

/* My constants. */
#define WORD_LENGTH 20
#define PROMPT_LENGTH 80
#define PLAYER_ONE 1
#define PLAYER_TWO 2
#define COMPUTER_PLAYER 3
#define MARKED 1
#define UNUSED 0

/* Function prototypes. */
void onePlayerGame(DictionaryType* dictionary);
void twoPlayerGame(DictionaryType* dictionary);
void dictionarySummary(DictionaryType* dictionary);
void searchDictionary(DictionaryType* dictionary);
void addDictionaryWord(DictionaryType* dictionary);
int saveDictionary(DictionaryType* dictionary, char* filename);

/* My prototypes */
void resetGame(DictionaryType *dictionary);
int findWord(DictionaryType* dictionary, char *enteredWord);
void userAddedWord(DictionaryType* dictionary, char *enteredWord, int result);
void clearStream(char *string);
char getLastChar(char *string);
char *removeNewline(char *string);
int playerGuess(DictionaryType* dictionary, char *enteredWord);
int computerGuess(DictionaryType *dictionary, char lastLetter);
int playerPrompt(DictionaryType* dictionary, char *prompt);
void displayMenu();
int compare (const void *a, const void *b); 

#endif
