/****************************************************************************
* COSC2138/CPT220 - Programming Principles 2A
* SP2 2015 Assignment #2 - word link program
* Full Name        : Paolo Felicelli
* Student Number   : s3427174
****************************************************************************/

/* file: list.h 
* 
* WordLinkType list
*
* type: WordLinkType
*
* constants: Uses the system-wide constants from wordlink.h
*
* interface routines:
*
* int addList(DictionaryType *dictionary, char *word, 
*            int headWordsElement, int position)
*
*     addList() takes in parameters that are used for validation and 
*     testing to establish where a node is to be inserted. Based on the 
*     test results, one of the 3 node insert functions is called. It also
*     keeps track of how many nodes are inserted.
*     
*
* int insertAtHead(DictionaryType *dictionary, char *string, int headSlot)
*     
*     insertAtHead() inserts a node at the head of a list if the number
*     0 is randomly generated and returns success on completion.
*
*
* int insertIntoMiddle(DictionaryType *dictionary, char *string, 
*                                     int headSlot, int position)
*     
*     insertIntoMiddle() inserts a node into the middle of the list if 
*     a number less than the list size that is not a 0 is randomly
*     generated and returns success on completion.
*
*
* int insertAtEnd(DictionaryType *dictionary, char *string, int headSlot)
*     
*     insertAtEnd() inserts a node to the end of a list if a randomly
*     generated position is greater than list size and returns success on 
*     completion.
*
*
* int getRandNumber(int number)
*     
*     getRandomNumber() generates a random number that is used for 
*     randomly positioning node insertions when loading data.
*
*
* void displayList(DictionaryType *dictionary)
*     
*     displayList() is used as a diagnostic testing function to print out
*     the contents of the linked lists to test whether they behave as 
*     expected after the data has been loaded.
*
*/ 

#ifndef LIST_H
#define LIST_H

typedef struct wordLinkStruct
{
   char* word;
   unsigned usedFlag;
   WordLinkTypePtr nextWord;
} WordLinkType;

/* Interface function prototypes */
int addList(DictionaryType *dictionary, char *word, 
            int headWordsElement, int position);
int insertAtHead(DictionaryType *dictionary, char *string, int headSlot);
int insertIntoMiddle(DictionaryType *dictionary, char *string, 
                                 int headSlot, int position);
int insertAtEnd(DictionaryType *dictionary, char *string, int headSlot);
void displayList(DictionaryType *dictionary);
int getRandNumber(int number);

#endif
