/****************************************************************************
* COSC2138/CPT220 - Programming Principles 2A
* SP2 2015 Assignment #2 - word link program
* Full Name        : Paolo Felicelli
* Student Number   : s3427174
* 
* Start up code provided by the C Teaching Team
****************************************************************************/

-----------------------------------------------------------------------------
If selected, do you grant permission for your assignment to be released as an
anonymous student sample solution?
-----------------------------------------------------------------------------

Yes

-----------------------------------------------------------------------------
Documentation of my implemented requirements:
-----------------------------------------------------------------------------

Requirement  1 - Command line args   : Yes
Requirement  2 - Load data           : Yes
Requirement  3 - Main menu           : Yes
Requirement  4 - 1 player game       : Yes
Requirement  5 - 2 player game       : Yes
Requirement  6 - Dictionary summary  : Yes
Requirement  7 - Search dictionary   : Yes
Requirement  8 - Add dictionary word : Yes
Requirement  9 - Save dictionary     : Yes
Requirement 10 - Exit                : Yes
Requirement 11 - Makefile            : Yes
Requirement 12 - Memory leaks        : Yes
Requirement 13 - Buffer handling     : Yes
Requirement 14 - Input validation    : Yes
Requirement 15 - Coding conventions  : Yes

-----------------------------------------------------------------------------
Known bugs:
-----------------------------------------------------------------------------

None that I am aware of.

-----------------------------------------------------------------------------
Assumptions:
-----------------------------------------------------------------------------



-----------------------------------------------------------------------------
Any other notes for the marker:
-----------------------------------------------------------------------------

Words are sorted back to alphabetical/ascending order when save data is
called. This functionality was implemented using qsort().

Built with Sublime text editor on Mac OSX 10.8.5. XCode GCC compiler.
Thoroughly tested on my system and RMIT Linux system. 

