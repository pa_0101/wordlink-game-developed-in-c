/****************************************************************************
* COSC2138/CPT220 - Programming Principles 2A
* SP2 2015 Assignment #2 - word link program
* Full Name        : Paolo Felicelli
* Student Number   : s3427174
* 
* Start up code provided by the C Teaching Team
****************************************************************************/

#ifndef WORDLINK_H
#define WORDLINK_H

/* System-wide header files. */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <ctype.h>

/* System-wide constants. */
#define SUCCESS 1
#define FAILURE 0
#define TRUE 1
#define FALSE 0
#define ALPHABET_LEN 26
#define SIZE 40000
#define WORD_LENGTH 20

/* Constants for getInteger() */
#define INPUT_LENGTH 1
#define TEMP_STRING_LENGTH 1
#define EXTRA_CHARS 2

typedef struct wordLinkStruct* WordLinkTypePtr;

/* Structure definition. */
typedef struct dictionaryStruct
{
   /*Pointer to the head of rhe linked list*/
   WordLinkTypePtr headWords[ALPHABET_LEN];
   unsigned listCounts[ALPHABET_LEN];
   unsigned totalWords;
} DictionaryType;

/* My prototypes */
int getInteger(int* integer, unsigned length, int min, int max);

#endif
