/****************************************************************************
* COSC2138/CPT220 - Programming Principles 2A
* SP2 2015 Assignment #2 - word link program
* Full Name        : Paolo Felicelli
* Student Number   : s3427174
****************************************************************************/

/* file: list.c
* 
* WordLinkType List interface functions
*
*/

#include "wordlink.h"
#include "wordlink_options.h"
#include "wordlink_utility.h"
#include "list.h"

int addList(DictionaryType *dictionary, char *word, 
            int headWordsElement, int position)
{
   int result;
   static int listCnt = 0;

   /* If position is 0, add node to the head of the list */
   if(position == 0) 
   {  
      result = insertAtHead(dictionary, word, headWordsElement);
      /* Increment the count of the amount of nodes added to the list */
      listCnt = listCnt + result;
   }

   /* If the position is less than the list count, add to the middle of the 
      list at randomly generated position */
   if(position < listCnt && position != 0)
   {
      result = insertIntoMiddle(dictionary, word, headWordsElement, position);
      /* Increment the count of the amount of nodes added to the list */
      listCnt = listCnt + result;
   }

   /* If the position is greater than or equal to the current size, then
      insert the new node at the end of the list */
   if(position >= listCnt)
   {
      result = insertAtEnd(dictionary, word, headWordsElement);
      /* Increment the count of the amount of nodes added to the list */
      listCnt = listCnt + result;
   }

   /* Tally up the total amount of words in the dictionary */
   dictionary->totalWords++; 

   return SUCCESS;
}


int insertAtHead(DictionaryType *dictionary, char *string, int headSlot)
{
   WordLinkTypePtr head = NULL, newNode, current, previous;

   /* Malloc a new node */
   if((newNode = malloc(sizeof(WordLinkType))) == NULL) 
   {
      /* Print error and exit if there is an error opening the file */
      fprintf(stderr,"\nMemory Allocation for ListInsert failed!\n");
      fprintf(stderr,"Aborting data entry!\n");
      exit(EXIT_FAILURE);   
   }

   /* Assign word to the word member in the node */
   newNode->word = string; 
   /* Set newNode to point to NULL */
   newNode->nextWord = NULL;

   /* Set head to the ith headWordsElement of the headWords array */
   head = dictionary->headWords[headSlot];
   /* Set current to be the head and previous to NULL */
   current = head;
   previous = NULL;

   /* Point current to previous to maintain a handle 
      throughout the chain */
   previous = current;
   /* Add the newNode to the head of the list */
   dictionary->headWords[headSlot] = newNode; 
   /* Join the new node to node after it */
   newNode->nextWord = current;

   return SUCCESS;
}


int insertIntoMiddle(DictionaryType *dictionary, char *string, 
                                   int headSlot, int position)
{
   int node = 0;
   WordLinkTypePtr head = NULL, newNode, current, previous;

   /* malloc a new node */
   if((newNode = malloc(sizeof(WordLinkType))) == NULL) 
   {
      /* Print error and exit if there is an error opening the file */
      fprintf(stderr,"\nMemory Allocation for ListInsert failed!\n");
      fprintf(stderr,"Aborting data entry!\n");
      exit(EXIT_FAILURE);   
   }

   /* Assign word to the word member in the node */
   newNode->word = string; 
   /* Set newNode to point to NULL */
   newNode->nextWord = NULL;

   /* Set head to the ith headWordsElement of the headWords array */
   head = dictionary->headWords[headSlot];
   /* Set current to be the head and previous to NULL */
   current = head;
   previous = NULL;

   /* While not at the end of the list */
   while(current != NULL)
   {
      /* Point current to previous to maintain a handle 
      throughout the chain */
      previous = current; 
      /* Point to the next word */
      current = current->nextWord;
      /* Count the nodes as we traverse up the list */
      node++;

      /* Check if were up to where we want to insert the new node */
      if(node == position)
      {
         /* Point the next pointer of the prev node to new node */
         previous->nextWord = newNode;
         /* Point the next pointer of the new node to current node */
         newNode->nextWord = current;
      }
   }

   return SUCCESS;
}


int insertAtEnd(DictionaryType *dictionary, char *string, int headSlot)
{
   WordLinkTypePtr head = NULL, newNode, current, previous;

   /* malloc a new node */
   if((newNode = malloc(sizeof(WordLinkType))) == NULL) 
   {
      /* Print error and exit if there is an error opening the file */
      fprintf(stderr,"\nMemory Allocation for ListInsert failed!\n");
      fprintf(stderr,"Aborting data entry!\n");
      exit(EXIT_FAILURE);   
   }

   /* Assign word to the word member in the node */
   newNode->word = string; 
   /* Set newNode to point to NULL */
   newNode->nextWord = NULL;

   /* Set head to the ith headWordsElement of the headWords array */
   head = dictionary->headWords[headSlot];
   /* Set current to be the head and previous to NULL */
   current = head;
   previous = NULL;

   /* While not at the end of the list */
   while(current != NULL)
   {
      /* Point current to previous to maintain a handle *
      throughout the chain */
      previous = current; 
      /* Point to the next word */
      current = current->nextWord;
   }

   /* Point the next pointer of the prev node to new node */
   previous->nextWord = newNode; 

   return SUCCESS;
}


int getRandNumber(int number)
{
   int randNum;

   /* Genarate a random number and store into randPosition */
   srand(time(0));
   randNum = rand() % number;

   return randNum;
}


void displayList(DictionaryType *dictionary) 
{ 
   WordLinkTypePtr current, head = NULL;
   int i;
  
   /* Loop through each element of the headWords array */
   for(i = 0; i < ALPHABET_LEN; i++)
   {  
      /* Assign each element to the head pointer */
      head = dictionary->headWords[i]; 
      /* Set current to head */
      current = head;
   
      /* Loop through the linked list and print the contents of the node */
      while(current != NULL) 
      { 
         printf("%s", current->word);
         /* Point to the next word */
         current = current->nextWord;
      } 
   }
}  
