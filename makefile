#############################################################################
# COSC2138/CPT220 - Programming Principles 2A
# SP2 2015 Assignment #2 - word link program
# Full Name        : Paolo Felicelli
# Student Number   : s3427174
# 
# Start up code provided by the C Teaching Team
#############################################################################

make: wordlink.c wordlink_utility.c wordlink_options.c
	gcc -ansi -pedantic -Wall -g -o wordlink wordlink.c wordlink_utility.c \
	wordlink_options.c list.c

clean:
	-rm -f *.o core

archive:
	zip $(USER)-a2.zip wordlink.c wordlink.h wordlink_options.c \
	wordlink_options.h wordlink_utility.c wordlink_utility.h \
	list.c list.h makefile readme.txt
